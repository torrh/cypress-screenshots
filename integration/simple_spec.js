describe('Pruebe el login correcto', function () {
    it('Pruebe el login correcto', function () {
        cy.visit('https://losestudiantes.co');
        cy.contains('Cerrar').click();
        cy.contains('Ingresar').click();
        cy.get('.cajaLogIn').find('input[name="correo"]').click().type("ha.torres11@uniandes.edu.co");
        cy.get('.cajaLogIn').find('input[name="password"]').click().type("123456789");
        cy.get('.cajaLogIn').contains('Ingresar').click();
        cy.get('#cuenta', { timeout: 2000 }).should((container) => {
            expect(container).to.exist
        })

        cy.screenshot('snapshot_login_1')
    })
})

describe('Pruebe el login correcto', function () {
    it('Pruebe el login correcto', function () {
        cy.visit('https://losestudiantes.co');
        cy.contains('Cerrar').click();
        cy.contains('Ingresar').click();
        cy.get('.cajaLogIn').find('input[name="correo"]').click().type("torrh85@gmail.com");
        cy.get('.cajaLogIn').find('input[name="password"]').click().type("123456789");
        cy.get('.cajaLogIn').contains('Ingresar').click();
        cy.get('#cuenta', { timeout: 2000 }).should((container) => {
            expect(container).to.exist
        })

        cy.screenshot('snapshot_login_2')
    })
})

describe('Crear cuenta existente', function () {
    it('Crear cuenta existente', function () {
        cy.visit('https://losestudiantes.co');
        cy.contains('Cerrar').click();
        cy.contains('Ingresar').click();
        cy.get('.cajaSignUp').find('input[name="nombre"]').click().type("Daniel");
        cy.get('.cajaSignUp').find('input[name="apellido"]').click().type("Ospina");
        cy.get('.cajaSignUp').find('input[name="correo"]').click().type("sdani@hotmail.com");
        cy.get('.cajaSignUp').find('select[name="idUniversidad"]').select("Universidad Nacional");
        cy.get('.cajaSignUp').find('select[name="idPrograma"]').select("Arte");
        cy.get('.cajaSignUp').find('input[name="password"]').click().type("123456789");

        cy.screenshot('snapshot_account_1')

    })
});

describe('Crear cuenta existente', function () {
    it('Crear cuenta existente', function () {
        cy.visit('https://losestudiantes.co');
        cy.contains('Cerrar').click();
        cy.contains('Ingresar').click();
        cy.get('.cajaSignUp').find('input[name="nombre"]').click().type("Alberto");
        cy.get('.cajaSignUp').find('input[name="apellido"]').click().type("Garcia");
        cy.get('.cajaSignUp').find('input[name="correo"]').click().type("mdi@hotmail.com");
        cy.get('.cajaSignUp').find('select[name="idUniversidad"]').select("Universidad de los Andes");
        cy.get('.cajaSignUp').find('select[name="idPrograma"]').select("Administración");
        cy.get('.cajaSignUp').find('input[name="password"]').click().type("34345345");

        cy.screenshot('snapshot_account_2')

    })
});


describe('Buscar profesor', function () {
    it('Buscar profesor', function () {
        cy.visit('https://losestudiantes.co');
        cy.contains('Cerrar').click();
        cy.get('.Select-control').click()
        cy.focused().type("Mario Linares Vasquez", { force: true });
        cy.get('.Select-menu-outer', { timeout: 2000 }).contains('Mario Linares Vasquez - Ingeniería de Sistemas', { timeout: 2000 }).should((container) => {
            expect(container).to.exist
        })

        cy.screenshot('snapshot_search_1')
    })
});


describe('Buscar profesor', function () {
    it('Buscar profesor', function () {
        cy.visit('https://losestudiantes.co');
        cy.contains('Cerrar').click();
        cy.get('.Select-control').click()
        cy.focused().type("Maria Margarita Botero de Meza ", { force: true });
        cy.get('.Select-menu-outer', { timeout: 2000 }).contains('Maria Margarita Botero de Meza - Matemáticas', { timeout: 2000 }).should((container) => {
            expect(container).to.exist
        })

        cy.screenshot('snapshot_search_2')
    })
});

